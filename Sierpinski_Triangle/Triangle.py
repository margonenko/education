__author__ = 'Igor Peresunko'

import turtle
wn = turtle.Screen()
igor = turtle.Turtle()

def middle(point_1, point_2):
    """Recalculates the midpoint of the segment by entering the coordinates of the previous point."""
    return ( (point_1[0] + point_2[0]) / 2, (point_1[1] + point_2[1]) / 2)

def draw_triangle(start, t):
    """Draws a triangle on the input coordinates."""
    t.up()
    t.goto(start[0][0], start[0][1])
    t.down()
    t.goto(start[1][0], start[1][1])
    t.goto(start[2][0], start[2][1])
    t.goto(start[0][0], start[0][1])

def computation_new_points(s_points,degree, t):
    """
    Calculates the new coordinates of mid points of the segments.
    Iterative method.
    """
    draw_triangle(s_points,t)
    if degree > 0:
        computation_new_points([s_points[0],
                               middle(s_points[0], s_points[1]),
                               middle(s_points[0], s_points[2])],
                               degree-1, t)
        computation_new_points([s_points[1],
                               middle(s_points[0], s_points[1]),
                               middle(s_points[1], s_points[2])],
                               degree-1, t)
        computation_new_points([s_points[2],
                                middle(s_points[2], s_points[1]),
                                middle(s_points[0], s_points[2])],
                                degree-1, t)

if __name__ == '__main__':
    degree = int(raw_input("Enter degrees:\n"))
    start_points = [ [-100,-50], [0,100], [100,-50] ]
    computation_new_points(start_points, degree, igor)

    wn.exitonclick()