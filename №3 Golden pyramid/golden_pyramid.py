__author__ = 'Igor Peresunko'
import random

def generate_random_number():
    """Generate random numbers to fill the pyramid."""
    n = random.randrange(1,10)
    return n

def filling_numbers(steps):
    """Fill the pyramid with random numbers."""
    pyramid = []
    for i in range(steps):
        line = []
        for x in range(i + 1):
            line = line + [generate_random_number()]
        pyramid.append(line)
    return pyramid

def golden_pyramid_d(triangle):
    """Found a solution using dynamic programming, disassembled for yourself"""
    for y in range(len(triangle) - 2, -1, -1):
        print(y)
        for x in range(y + 1):
            triangle[y][x] += max(triangle[y + 1][x], triangle[y + 1][x + 1])
    return triangle[0][0]

def golden_pyramid_a(triangle):
    revers_pyramid = []
    for i in range(len(triangle) -1, -1, -1):
        revers_pyramid.append(triangle[i][:])
    print(revers_pyramid)

    for y in range(0, len(revers_pyramid)):
        print('y', y)
        for x in range(y + 1):
            print(revers_pyramid[y][x])





if __name__ == '__main__':
    numb_steps = int(raw_input('Enter the number of steps:\n'))
    pyramid = filling_numbers(numb_steps)

    for line in pyramid:
            print(line)

    print('*') * 20

    max_sum = golden_pyramid_a(pyramid)
    #max_sum = golden_pyramid_d(pyramid)
    print(max_sum)



