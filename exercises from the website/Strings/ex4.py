__author__ = 'Igor Peresunko'

def multiplication_table(stop):
    for i in range(1, stop + 1):
        pr_i = str(i)
        for x in range(1, stop + 1):
            pr_x = str(x)
            result = str(i * x)
            print(pr_i + " * " + pr_x + " = " + result)
        print(" ")

multiplication_table(12)

