__author__ = 'Igor Peresunko'

def parse_text(string, ch):
    length_of_string = len(string)
    count_ch = 0
    for i in string:
        if i == ch:
            count_ch += 1

    percent = round((float(count_ch) / length_of_string) * 100, 2)
    str_length = str(length_of_string)
    str_percent = str(percent)
    str_count = str(count_ch)
    print("Your text contains " + str_length + " alphabetic characters, of which " + str_count + " (" + str_percent + ") are - " + ch)

string = 'Hello my dear hello my friend'
parse_text(string, 'e')
