__author__ = 'Igor Peresunko'

def root13(string):
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    new_str = ''
    for i in string:
        index = string.find(i)
        new_index = index + 13
        if new_index > 26:
            new_index = new_index % 26
            new_str = new_str + alphabet[new_index]
        else:
            new_str = new_str + alphabet[new_index]

    return new_str

print(root13(root13('abc abc')))

