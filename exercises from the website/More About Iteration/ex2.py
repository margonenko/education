__author__ = 'user'

def print_triangular_numbers(n):
    result = n*(n+1)/2
    return result

n = 5
for i in range(1,n + 1):
    a = print_triangular_numbers(i)
    print i, '\t', a