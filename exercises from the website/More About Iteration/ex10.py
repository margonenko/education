__author__ = 'user'
import image

img = image.Image("luther.jpg")
win = image.ImageWin(img.getWidth(), img.getHeight())
img.draw(win)

def remove_red():
    for col in range(img.getWidth()):
        for row in range(img.getHeight()):
            p = img.getPixel(col, row)
            
            red = int(p.getRed() * 0.393 + p.getGreen() * 0.769 + p.getBlue() * 0.189)
            green = int(p.getRed() * 0.349 + p.getGreen() * 0.686 + p.getBlue() * 0.168)
            blue = int(p.getRed() * 0.272 + p.getGreen() * 0.534 + p.getBlue() * 0.131)

            newpixel = image.Pixel(red, green, blue)
            img.setPixel(col, row, newpixel)
            
            img.setPixel(col, row, newpixel)

remove_red()


