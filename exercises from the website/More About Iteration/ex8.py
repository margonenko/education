__author__ = 'user'
import image

img = image.Image("luther.jpg")
win = image.ImageWin(img.getWidth(), img.getHeight())
img.draw(win)

def remove_red():
    for col in range(img.getWidth()):
        for row in range(img.getHeight()):
            p = img.getPixel(col, row)
            
            red = p.getRed()
            green = p.getGreen()
            blue = p.getBlue()
            
            avg = (red + green + blue) / 3.0
            
            newpixel = image.Pixel(avg, avg, avg)
            img.setPixel(col, row, newpixel)
            
            img.setPixel(col, row, newpixel)

remove_red()


