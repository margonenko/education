__author__ = 'user'
import random
import turtle

def isInScreen(w,t,b):
    leftBound = - w.window_width() / 2
    rightBound = w.window_width() / 2
    topBound = w.window_height() / 2
    bottomBound = -w.window_height() / 2

    turtleX = t.xcor()
    turtleY = t.ycor()

    stillIn = True
    if turtleX > rightBound or turtleX < leftBound:
        stillIn = False
    if turtleY > topBound or turtleY < bottomBound:
        stillIn = False

    turtleBX = b.xcor()
    turtleBY = b.ycor()

    stillIn = True
    if turtleBX > rightBound or turtleBX < leftBound:
        stillIn = False
    if turtleBY > topBound or turtleBY < bottomBound:
        stillIn = False

    return stillIn

def randAngle():
    return random.randrange(0, 360)

def rand_position():
    x = random.randrange(0, 100)
    y = random.randrange(0, 100)
    return x,y

t = turtle.Turtle()
b = turtle.Turtle()
wn = turtle.Screen()

t.shape('turtle')
t.penup()
b.shape('turtle')
b.penup()

t.goto(rand_position())
b.goto(rand_position())
t.pendown()
b.pendown()

while isInScreen(wn,t, b):
    coin = random.randrange(0, 2)
    if coin == 0:
        t.left(randAngle())
        b.left(randAngle())
    else:
        t.right(randAngle())
        b.right(randAngle())
    t.forward(50)
    b.forward(50)
wn.exitonclick()