__author__ = 'user'
import image

img = image.Image("luther.jpg")
win = image.ImageWin(img.getWidth(), img.getHeight())
img.draw(win)

def remove_red():
    for col in range(img.getWidth()):
        for row in range(img.getHeight()):
            p = img.getPixel(col, row)
            
            newred = 0
            newgreen = p.getGreen()
            newblue = p.getBlue()

            newpixel = image.Pixel(newred, newgreen, newblue)

            img.setPixel(col, row, newpixel)

remove_red()
