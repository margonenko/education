__author__ = 'Igor Peresunko'

def date(year):
    if 1900 < year < 2099:
        a = year % 19
        b = year % 4
        c = year % 7
        d = (19 * a + 24) % 30
        e = (2 * b + 4 * c + 6 * d + 5) % 7
        dateofeaster = 22 + d + e
        return dateofeaster
    else:
        print("Error")


d = date(2014)
print(d)