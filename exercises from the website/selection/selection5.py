__author__ = 'Igor Peresunko'
import turtle

wn = turtle.Screen()
igor = turtle.Turtle()
igor.speed(5)

def drawBar(t, height):
    """ Get turtle t to draw one bar, of height. """
    t.begin_fill()
    t.left(90)
    t.forward(height)
    t.write(str(height))
    t.right(90)
    t.forward(40)
    t.right(90)
    t.forward(height)
    t.left(90)
    t.end_fill()

xs = [120, 117, 280, 240, -160, 198, 220]

igor = turtle.Turtle()
igor.color("blue")
igor.pensize(3)

for a in xs:
    if a >= 200:
        igor.fillcolor("red")
        drawBar(igor, a)
    elif 100 <= a <= 200:
        igor.fillcolor("green")
        drawBar(igor,a)
    elif a < 0:
        igor.fillcolor("lightgreen")
        drawBar(igor,a)

wn.exitonclick()