__author__ = 'user'
import math


def is_rightangled(a, b, c):

    if a > b and a > c:
        is_rightangled = abs(b**2 + c**2 - a**2) < 0.001
    elif b > a and b > c:
        is_rightangled = abs(a**2 + c**2 - b**2) < 0.001
    else:
        is_rightangled = abs(a**2 + b**2 - c**2) < 0.001
    return is_rightangled

a = 1.5
b = 2.0
c = 2.5

r = is_rightangled(a,b,c)
print(r)