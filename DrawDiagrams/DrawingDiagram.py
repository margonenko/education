__author__ = 'Igor Peresunko'
import turtle
import  itertools

wn = turtle.Screen()
igor = turtle.Turtle()
igor.speed(5)

dictionary_two = {}
dictionary_three = {}

def analysis(str_phrase, dic_2):
    """Carries the words from the list dic_1 in the dictionary dic_2 and makes them count"""
    dictionary_one = str_phrase.split(" ")
    for word in dictionary_one:
        if word not in dic_2:
            dic_2[word] = 1
        else:
            dic_2[word] += 1
    return dic_2

def draw_ray_diagram(dic_2, t):
    """Draws a ray diagram"""
    count_words = len(dic_2)
    angle = 360 / count_words

    color = ['red', 'blue', 'green', 'purple', 'brown', 'magenta']
    ci = itertools.cycle(color)

    for i in dic_2:
        line = dic_2.setdefault(i)
        t.color(ci.next())
        for x in range(line):
            t.forward(40)
            t.circle(3)
        write_word(t,i)
        t.left(180)
        t.forward(line * 40)
        t.left(angle + 180)

def write_word(t, word):
    """Indents, type a word and returns the handle to the end of line"""
    t.penup()
    t.forward(20)
    t.write(word)
    t.left(180)
    t.forward(20)
    t.left(180)
    t.pendown()

def draw_sector_diagram(dict_2, dict_3, t):
    """Draws a pie chart"""
    total_amount = 0
    color = ['red', 'blue', 'green', 'purple', 'brown', 'magenta']
    ci = itertools.cycle(color)
    cl = itertools.cycle(color)
    for i in dict_2:
        total_amount += dict_2.setdefault(i)

    for x in  dict_2:
        dict_3[x] = float(dict_2.setdefault(x)) / float(total_amount)

    for y in dict_3:
        dict_3[y] = int(dict_3.setdefault(y) * 360.0)
    print(dict_2)
    print(dict_3)
    for j in dict_3:
        x = dict_3.setdefault(j)
        t.color(ci.next())
        draw_sector(igor, x)

    t.penup()
    t.goto(150, 100)

    for h in dict_2:
        value = dict_2.setdefault(h)
        t.color(cl.next())
        sector_write_word(t, h, value)
        t.right(90)
        t.forward(40)
        t.left(90)


def draw_sector(t, angle):
    """Draws a sector of a pie chart"""
    t.begin_fill()
    t.forward(90)
    t.left(90)
    t.circle(90, angle)
    t.left(90)
    t.forward(90)
    t.end_fill()
    t.left(180)

def sector_write_word(t, word, value):
    """Displays a pie chart legend"""
    key = str(word)
    val = str(value)
    phrase = key + ' - ' + val + 'time(s)'

    t.pendown()
    t.dot(20)
    t.penup()
    t.forward(20)
    t.pendown()
    t.write(phrase)
    t.penup()
    t.left(180)
    t.forward(20)
    t.left(180)

def draw_diagram(type, dict_2, dict_3, t):
    """Selects the chart type for input value"""
    if type == '2':
        draw_ray_diagram(dict_2, t)
    elif type == '1':
        draw_sector_diagram(dict_2, dict_3, t)

diagram = raw_input("Enter type of diagram (1 - sector, 2 - ray):\n")
str_phrase = raw_input("Enter your phrase\n")

dictionary_two = analysis(str_phrase, dictionary_two)
draw_diagram(diagram, dictionary_two, dictionary_three, igor)

wn.exitonclick()