__author__ = 'Igor Peresunko'

from shape import *


class Circle(Shape):
    """Draw a circle"""

    def draw(self):
        self.turtle.color(self.fill_color)
        self.turtle.begin_fill()
        self.turtle.circle(self.size)
        self.turtle.end_fill()


class Square(Shape):
    """Draw a square"""

    def draw(self):
        self.turtle.color(self.fill_color)
        self.turtle.begin_fill()
        for i in range(4):
            self.turtle.forward(self.size)
            self.turtle.left(90)
        self.turtle.end_fill()


class Polygon(Shape):
    """Draw a polygon"""

    def __init__(self, pen_size, fill_color, size, position, count_of_angle):
        Shape.__init__(self, pen_size, fill_color, size, position)
        self.count_of_angle = count_of_angle

    def draw(self):
        """Draws a polygon defined by the number of angles"""

        angle = 360 / self.count_of_angle
        self.turtle.color(self.fill_color)
        self.turtle.begin_fill()
        for i in range(self.count_of_angle):
            self.turtle.forward(self.size)
            self.turtle.left(angle)
        self.turtle.end_fill()


class Triangle(Shape):
    """Draw a triangle"""

    def draw(self):
        self.turtle.color(self.fill_color)
        self.turtle.begin_fill()
        for i in range(3):
            self.turtle.forward(self.size)
            self.turtle.left(120)
        self.turtle.end_fill()
