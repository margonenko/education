__author__ = 'Igor Peresunko'


class Canvas():
    """
    Store parameters about figure
    """
    def __init__(self):
        self.canvas = []
        self.list_param = {}

    def save_param(self, size, color,  position, type):
        """Retains the shape options in the list"""
        self.parameter_list = [size, position,
                               color, type]
        self.canvas.append(self.parameter_list)
        return self.canvas

    def to_str(self, size, position, color):
        return str(size) + str(position) + color
