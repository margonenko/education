__author__ = 'Igor Peresunko'

from figure import *
from canvas import *

param = Canvas()

a1 = Circle(5, 'pink', 90, [30, 40])
param.save_param(a1.size, a1.fill_color, a1.position, 'c')
a1.draw()

a2 = Square(6, 'green', 90, [100, 30])
param.save_param(a2.size, a2.fill_color, a2.position, 's')
a2.draw()

a3 = Polygon(7, 'brown', 45, [-100, 50], 8)
param.save_param(a3.size, a3.fill_color, a3.position, 'p')
a3.draw()

a4 = Triangle(2, 'red', 70, [-150, -150])
param.save_param(a4.size, a4.fill_color, a4.position, 't')
a4.draw()

print(param.canvas)

f = open('test.txt', 'a')
for i in range(len(param.canvas)):
    to_file = str(param.canvas[i])
    f.write(to_file + '\n')
    print(param.canvas[i])
f.close()
a1.stop()
