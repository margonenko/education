__author__ = 'Igor Peresunko'
import turtle


class Shape():
    """Contains the basic essence"""

    def __init__(self, pen_size, fill_color, size, position):
        self.pen_size = pen_size
        self.fill_color = fill_color
        self.size = size
        self.position = position

        self.turtle = turtle.Turtle()
        self.wn = turtle.Screen()
        self.change_of_position(self.position)

    def change_of_position(self, position):
        """A change of coordinates turtles"""
        self.turtle.penup()
        self.turtle.goto(position[0], position[1])
        self.turtle.pendown()

    def stop(self):
        """Displays the image until the mouse button is pressed"""

        self.wn.exitonclick()
