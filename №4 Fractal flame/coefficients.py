__author__ = 'Igor Peresunko'
import random

class Coeff:
    """Generation of starting coefficients"""

    def sum_of_sqrt(self, a, b):
        """Returns the sum of the squares of the two numbers"""

        result = a ** 2 + b ** 2
        return result

    def __init__(self):
        self.a = random.uniform(-1, 1)
        self.b = random.uniform(-1, 1)
        self.d = random.uniform(-1, 1)
        self.e = random.uniform(-1, 1)

        self.c = random.uniform(-1.5, 1.5)
        self.f = random.uniform(-1.5, 1.5)

        self.R = random.randrange(0, 256)
        self.G = random.randrange(0, 256)
        self.B = random.randrange(0, 256)

    def test_conditions(self):
        """Check coefficients for passage conditions"""

        if self.sum_of_sqrt(self.a, self.d) < 1 and self.sum_of_sqrt(self.b, self.e) < 1 and \
           self.sum_of_sqrt(self.a, self.b) + self.sum_of_sqrt(self.b, self.e) < 1 + \
                                (self.a * self.e - self.b * self.d) ** 2:
            return True
        else:
            return False

    def to_list(self, obj):
        """In the case of passing the condition is all the variables"""

        while not obj.test_conditions():
            return self.to_list(obj)
        else:
            return [self.a, self.b, self.d, self.e, self.c, self.f, [self.R, self.G, self.B]]



a = Coeff()
list_of_coefficient = a.to_list(a)
print(list_of_coefficient)